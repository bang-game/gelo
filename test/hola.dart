class PubSpec {
  String version;
  List<String> _dependencies;

  List<String> get dependencies => _dependencies;

  set dependencies(List<String> value) {
    _dependencies = value;
  }

}

main() {
  var pubSpec = PubSpec();
  pubSpec.version = "1";
  pubSpec._dependencies = [ "one", "two"];

  print(pubSpec._dependencies);
  print(pubSpec.version);
}
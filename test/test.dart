import 'dart:io';
import 'dart:math';

main() {
  var a, b, c, d;

  print("введиете числа для уравнения a*х^2+b*x+c");
  print("введите a: ");
  a = int.parse(stdin.readLineSync());
  print("введите b: ");
  b = int.parse(stdin.readLineSync());
  print("введите c: ");
  c = int.parse(stdin.readLineSync());
  print("у вас получилось $a * х ^ 2 + $b * x + $c");

  d = b * b - 4 * a * c;

  print("discriminant");
  print(d);

  if (d > 0) {
    print("x1 = ${(-b - sqrt(d)) / 2 * a}");
    print("x2 = ${(-b + sqrt(d)) / 2 * a}");
  } else if (d == 0) {
    print("x1 = ${-b / 2 * a}");
  } else {
    // ignore: unnecessary_statements
    print("no");
  }
}
